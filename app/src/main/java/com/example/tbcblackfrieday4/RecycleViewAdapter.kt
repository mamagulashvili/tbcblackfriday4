package com.example.tbcblackfrieday4

import android.content.Context
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcblackfrieday4.databinding.RowItemBinding

class RecycleViewAdapter(
    val buttonAmount: MutableList<Int>,
    val onImageClick: OnImageClick,
    val context: Context
) :
    RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder>() {

    private var playerOne = true
    private var clickCounter = 0
    private val playerOneClickList = mutableListOf<Int>()
    private val playerTwoClickList = mutableListOf<Int>()
    private lateinit var winner:String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = buttonAmount.size

    inner class MyViewHolder(val binding: RowItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            binding.buttonImage.setOnClickListener {
                clickCounter++
                if (playerOne) {
                    binding.apply {
                        buttonImage.setImageResource(R.drawable.drawable_x)
                    }
                    winner = "PlayerOne"
                    playerOneClickList.add(buttonAmount[adapterPosition])
                } else {
                    binding.buttonImage.setImageResource(R.drawable.drawable_o)
                    playerTwoClickList.add(buttonAmount[adapterPosition])
                    winner = "PlayerTwo"
                }
                onImageClick.onImageClick(it)
                onImageClick.getWinner(clickCounter,playerOneClickList,winner)
                onImageClick.getWinner(clickCounter,playerTwoClickList,winner)

                playerOne = !playerOne

            }

        }
    }
}