package com.example.tbcblackfrieday4

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.example.tbcblackfrieday4.Constants.KEY_CODE

import com.example.tbcblackfrieday4.Constants.REQUEST_CODE
import com.example.tbcblackfrieday4.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding: FragmentMainBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        init()
        return _binding!!.root
    }

    private fun init() {
        binding.apply {
            btnSelect3x3.setOnClickListener {
                findNavController().navigate(R.id.action_mainFragment_to_gameFragment)
                setFragmentResult(
                    REQUEST_CODE,
                    bundleOf(KEY_CODE to 9)
                )
            }
            btnSelect4x4.setOnClickListener {
                findNavController().navigate(R.id.action_mainFragment_to_gameFragment)
                setFragmentResult(
                    REQUEST_CODE,
                    bundleOf(KEY_CODE to 16)
                )
            }
            btnSelect5x5.setOnClickListener {
                findNavController().navigate(R.id.action_mainFragment_to_gameFragment)
                setFragmentResult(
                    REQUEST_CODE,
                    bundleOf(KEY_CODE to 25)
                )
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}