package com.example.tbcblackfrieday4

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.tbcblackfrieday4.Constants.KEY_CODE
import com.example.tbcblackfrieday4.Constants.REQUEST_CODE
import com.example.tbcblackfrieday4.databinding.FragmentGameBinding
import com.google.android.material.snackbar.Snackbar

class GameFragment : Fragment() {

    private var _binding: FragmentGameBinding? = null
    private val binding: FragmentGameBinding get() = _binding!!
    private lateinit var rvAdapter: RecycleViewAdapter
    private var buttonAmount = mutableListOf<Int>()
    private var quantity: Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGameBinding.inflate(layoutInflater, container, false)
        init()
        return _binding!!.root
    }

    private fun init() {

        setFragmentResultListener(REQUEST_CODE) { _, bundle ->
            val result = bundle.getInt(KEY_CODE)
            setRecycleView(result)
            quantity = result
        }
    }

    private fun setRecycleView(gridSpanCounter: Int) {
        val onImageClick = object : OnImageClick {
            override fun onImageClick(button: View) {
                button.isClickable = false
            }

            override fun getWinner(
                click: Int,
                playerClickList: MutableList<Int>,
                winner: String,
            ) {
                winner(click, playerClickList, winner)
            }


        }
        for (i in 1..gridSpanCounter) {
            buttonAmount.add(i)
        }
        rvAdapter = RecycleViewAdapter(buttonAmount, onImageClick, requireContext())
        when (gridSpanCounter) {
            9 -> {
                binding.rvGame.apply {
                    layoutManager = GridLayoutManager(context, 3)
                    adapter = rvAdapter
                }
            }
            16 -> {
                binding.rvGame.apply {
                    layoutManager = GridLayoutManager(context, 4)
                    adapter = rvAdapter
                }
            }
            25 -> {
                binding.rvGame.apply {
                    layoutManager = GridLayoutManager(context, 5)
                    adapter = rvAdapter
                }
            }
        }
    }

    fun winner(
        clickCounter: Int,
        playerClickList: MutableList<Int>,
        winner: String,
    ) {


        val nineBtnVerticalWinner1 = mutableListOf(1, 4, 7)
        val nineBtnVerticalWinner2 = mutableListOf(2, 5, 8)
        val nineBtnVerticalWinner3 = mutableListOf(3, 6, 9)
        val nineBtnHorizontalWinner1 = mutableListOf(1, 2, 3)
        val nineBtnHorizontalWinner2 = mutableListOf(4, 5, 6)
        val nineBtnHorizontalWinner3 = mutableListOf(7, 8, 9)
        val nineBtnDiagonalWinner1 = mutableListOf(1, 5, 9)
        val nineBtnDiagonalWinner2 = mutableListOf(3, 5, 7)

        val sixteenBtnVerticalWinner1 = mutableListOf(1,5,9,13)
        val sixteenBtnVerticalWinner2 = mutableListOf(2,6,10,14)
        val sixteenBtnVerticalWinner3 = mutableListOf(3,7,11,15)
        val sixteenBtnVerticalWinner4 = mutableListOf(4,8,12,16)
        val sixteenBtnHorizontalWinner1 = mutableListOf(1,2,3,4)
        val sixteenBtnHorizontalWinner2 = mutableListOf(5,6,7,8)
        val sixteenBtnHorizontalWinner3 = mutableListOf(9,10,11,12)
        val sixteenBtnHorizontalWinner4 = mutableListOf(13,14,15,16)
        val sixteenBtnDiagonalWinner1 = mutableListOf(1,6,11,16)
        val sixteenBtnDiagonalWinner2 = mutableListOf(4,7,10,13)



        when (quantity) {
            9 -> {
                if (playerClickList.containsAll(nineBtnVerticalWinner1) || playerClickList.containsAll(
                        nineBtnVerticalWinner2
                    ) || playerClickList.containsAll(nineBtnVerticalWinner3)
                    || (playerClickList.containsAll(nineBtnHorizontalWinner1)) || (playerClickList.containsAll(
                        nineBtnHorizontalWinner2
                    )) || (playerClickList.containsAll(nineBtnHorizontalWinner3))
                    || (playerClickList.containsAll(nineBtnDiagonalWinner1)) || (playerClickList.containsAll(
                        nineBtnDiagonalWinner2
                    ))
                ) {
                    displayWinner(winner)
                    findNavController().navigate(R.id.action_gameFragment_to_mainFragment)
                }
            }
            16 -> {
                if (playerClickList.containsAll(sixteenBtnVerticalWinner1) || playerClickList.containsAll(
                        sixteenBtnVerticalWinner2
                    ) || playerClickList.containsAll(sixteenBtnVerticalWinner3)
                    || (playerClickList.containsAll(sixteenBtnVerticalWinner4)) || (playerClickList.containsAll(
                        sixteenBtnHorizontalWinner1
                    )) || (playerClickList.containsAll(sixteenBtnHorizontalWinner2))
                    || (playerClickList.containsAll(sixteenBtnHorizontalWinner3)) || (playerClickList.containsAll(
                        sixteenBtnHorizontalWinner4
                    )) || playerClickList.containsAll(sixteenBtnDiagonalWinner1) || playerClickList.containsAll(sixteenBtnDiagonalWinner2)
                ) {
                    displayWinner(winner)
                    findNavController().navigate(R.id.action_gameFragment_to_mainFragment)
                }
            }
        }

        if (clickCounter == buttonAmount.size) {
            Snackbar.make(binding.root, "frea", Snackbar.LENGTH_SHORT).show()
        }

    }

    private fun displayWinner(winner: String) {
        Snackbar.make(binding.root, "Winner is $winner", Snackbar.LENGTH_SHORT)
            .show()
    }
}