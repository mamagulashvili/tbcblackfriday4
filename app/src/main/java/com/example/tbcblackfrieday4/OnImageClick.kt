package com.example.tbcblackfrieday4

import android.view.View
import android.widget.ImageButton

interface OnImageClick {
    fun onImageClick(button: View)
    fun getWinner(
        click: Int,
        playerClickList:MutableList<Int>,
        winner: String,
    )
}